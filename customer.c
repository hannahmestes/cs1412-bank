#include "customer.h"
#include "admin.h"
#include "user.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int login(struct user *userList, char customerId[], char password[]){
    int flag = 0;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, customerId) == 0){
            if (strcmp(p->password, password) == 0){
                flag = 1;
                return flag;
            }
            else{
                printf("Incorrect password.\n");
                return flag;
            }
        }
    }
    printf("No user with that ID found.\n");
    return flag;
}

void printCustomerBalance(struct user *userList, char userId[]){
    int flag = 0;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, userId) == 0){
            printf("%.2lf\n", p->balance);
            flag = 1;
        }
    }
    if (flag == 0){
        printf("No user with that id found. \n");
    }  
}

void makeDeposit(struct user *userList, char userId[], double amount){
    int flag = 0;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, userId) == 0){
            (p->balance) += amount;
            printf("New balance is: %.2lf\n", p->balance);
            flag = 1;
        }
    }
    if (flag == 0){
        printf("No user with that id found. \n");
    }  
}

void makeWithdrawal(struct user *userList, char userId[], double amount){
    int flag = 0;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, userId) == 0){
            if (p->balance >= amount){
                (p->balance) -= amount;
                printf("New balance is: %.2lf\n", p->balance);
            }
            else{
                printf("Insufficient funds, transaction aborted.\n");
            }
            flag = 1;
        }
    }
    if (flag == 0){
        printf("No user with that id found.\n");
    }  
}

void changePassword(struct user *userList, char userId[]){
    int flag = 0;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, userId) == 0){
            while(1){
                printf("Enter a password: ");
                fscanf(stdin, "%s", p->password);
                while(getchar() != '\n');
                if(strlen(p->password)<=6){
                    break;
                }
                printf("Password must be six characters or less!\n");
            }
            flag = 1;
        }
    }
    if (flag == 0){
        printf("No user with that id found.\n");
    }  
}

void makeTransfer(struct user *userList, char sourceId[], char destId[], double amount){
    int flagSourceAcct = 0;
    struct user *sourceAcct;
    struct user *destAcct;

    for(sourceAcct = userList; sourceAcct != NULL; sourceAcct = sourceAcct->next){
        if (strcmp(sourceAcct->id, sourceId) == 0){
            if (sourceAcct->balance >= amount){
                goto foundSource;
            }
            else{
                printf("Insufficient funds, transaction aborted.\n");
                return;
            }
            flagSourceAcct = 1;
        }
    }
    if (flagSourceAcct == 0){
        printf("No user found with source id.\n");
        return;
    }
    foundSource:for(destAcct = userList; destAcct != NULL; destAcct = destAcct->next){
        if(strcmp(destAcct->id, destId) == 0){
            (destAcct->balance)+= amount;
            (sourceAcct->balance) -= amount;
            return;
        }
    }
    printf("No user found with destination id.\n");
}