#include "admin.h"
#include "user.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define TRUE 1
#define FALSE 0

int count_lines(FILE customer_file);
void SortArray(double master[], int low, int high);
struct user *deleteVoidUser(struct user *userList);

/********************************************************************
 * This function will open the specified file, read the data and put*
 * the data into a linked list. The function returns a pointer to   *
 * the first item in the linked list.                               *
 ********************************************************************/
struct user *pullData(char fileName[]){
    FILE *customer_file;
    customer_file = fopen(fileName, "r+");
    if (customer_file == NULL){
        printf("Connection can't be opened. The application may already be in use\n");
        exit(EXIT_FAILURE);
    }
    else {
        struct user *first = NULL;

        while (!feof(customer_file)){
            struct user *new_user;
            new_user = malloc(sizeof(struct user));
            fscanf(customer_file, "%s", new_user->first_name);
            fscanf(customer_file, "%s", new_user->last_name);
            fscanf(customer_file, "%s", new_user->city);
            fscanf(customer_file, "%s", new_user->state);
            fscanf(customer_file, "%s", new_user->phone);
            fscanf(customer_file, "%s", new_user->id);
            fscanf(customer_file, "%s", new_user->password);
            fscanf(customer_file, "%lf", &new_user->balance);
            new_user->next = first;
            first = new_user;
        }
        fclose(customer_file);
        remove(fileName);
        first =deleteVoidUser(first);
        return first;
    }
}

/********************************************************************
 * Setter function for user first name.                             *
 ********************************************************************/
void set_first_name(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            printf("Enter first name: ");
            fscanf(stdin, "%8s", p->first_name);
            while(getchar() != '\n');
            return;
        }
    }
}

/********************************************************************
 * Setter function for user last name.                              *
 ********************************************************************/
void set_last_name(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            printf("Enter last name: ");
            fscanf(stdin, "%8s", p->last_name);
            while(getchar() != '\n');
            return;
        }
    }
}

/********************************************************************
 * Setter function for user city.                                   *
 ********************************************************************/
void set_city(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            printf("Enter the customer's city: ");
            fscanf(stdin, "%10s", p->city);
            while(getchar() != '\n');
            return;
        }
    }
}

/********************************************************************
 * Setter function for user state.                                  *
 ********************************************************************/
void set_state(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            printf("Enter the customer's state: ");
            fscanf(stdin, "%2s", p->state);
            while(getchar() != '\n');
            return;
        }
    }
}

/********************************************************************
 * Setter function for user phone number.                           *
 ********************************************************************/
void set_phone(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            printf("Enter the customer's phone number in format XXX-XXXX: ");
            fscanf(stdin, "%8s", p->phone);
            while(getchar() != '\n');
            return;
        }
    }
}

/********************************************************************
 * Setter function for user id.                                     *
 ********************************************************************/
void set_id(struct user *userList, char userId[]){
    if(strcmp("admin", userId) == 0){
        printf("Cannot change admin id!\n");
        return;
    }
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            setuserid: printf("Enter the customer's user id: ");
            fscanf(stdin, "%5s", p->id);
            while(getchar() != '\n');
            if (userExists(userList, p->id)){
            printf("User IDs must be unique! please enter a new id.\n");
            goto setuserid;
            }       
            return;
        }
    }
}

/********************************************************************
 * Setter function for user password with validation.               *
 ********************************************************************/
void set_password(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            while(1){
                printf("Enter a password: ");
                fscanf(stdin, "%s", p->password);
                while(getchar() != '\n');
                if(strlen(p->password)<=6){
                    break;
                }
                printf("Password must be six characters or less!\n");
            }
            return;
        }
    }
}

/********************************************************************
 * This function will open the specified file and write the data    *
 * contained in the linked list into the file specified. If the file*
 * does not exist, the file will be created.                        *
 ********************************************************************/

void writeData(char fileName[], struct user *userList)
{
    FILE *customer_file;
    customer_file = fopen(fileName, "a+");
    if (customer_file == NULL){
        exit(EXIT_FAILURE);
    }
    else {
        for(struct user *p = userList; p != NULL; p = p->next){
            fprintf(customer_file, "%s ", p->first_name);
            fprintf(customer_file, "%s ", p->last_name);
            fprintf(customer_file, "%s ", p->city);
            fprintf(customer_file, "%s ", p->state);
            fprintf(customer_file, "%s ", p->phone);
            fprintf(customer_file, "%s ", p->id);
            fprintf(customer_file, "%s ", p->password);
            fprintf(customer_file, "%.2lf ", p->balance);
            if (p->next != NULL){
                fprintf(customer_file, "\n");
            }
        }
    }
    fclose(customer_file);
}

/********************************************************************
 * This function uses a pointer to the file containing all users to *
 * return the number of customers in the file, not including the    *
 * admin user.                                                      *
 ********************************************************************/
int count_lines(FILE customer_file){
    int num_lines = 0;
    int ch;
    while(!feof(&customer_file)){
        ch = fgetc(&customer_file);
        if(ch == '\n'){
            num_lines++;
        }
    }   
    return num_lines;
}

/********************************************************************
 * This function accepts a pointer to a linked list of customers and*
 * a user ID, then checks to see if that user ID is in the linked   *
 * list. 1 is returned if the user is found, 0 is returned if the   *
 * user is not found.                                               *
 ********************************************************************/
int userExists(struct user *userList, char userId[]){
    for(struct user *p = userList; p != NULL; p = p->next){
        if(strcmp(p->id, userId) == 0){
            return 1;
        }
    }
    return 0;
}

/********************************************************************
 * This function will give all of the neccessary prompts to create  *
 * a new user structure, and then insert the user at the beginning  * 
 * of the linked list speciied.                                     *
 ********************************************************************/
struct user *createCustomer(struct user *userList){
    struct user *new_user;
    new_user = malloc(sizeof(struct user));

    printf("Enter first name: ");
    fscanf(stdin, "%8s", new_user->first_name);
    while(getchar() != '\n');
    printf("Enter last name: ");
    fscanf(stdin, "%8s", new_user->last_name);
    while(getchar() != '\n');
    printf("Enter the customer's city: ");
    fscanf(stdin, "%10s", new_user->city);
    while(getchar() != '\n');
    printf("Enter the customer's state: ");
    fscanf(stdin, "%2s", new_user->state);
    while(getchar() != '\n');
    printf("Enter the customer's phone number in format XXX-XXXX: ");
    fscanf(stdin, "%8s", new_user->phone);
    userid: while(getchar() != '\n');
    printf("Enter the customer's user id: ");
    fscanf(stdin, "%5s", new_user->id);
    while(getchar() != '\n');
    if (userExists(userList, new_user->id)){
        printf("User IDs must be unique! please enter a new id.\n");
        goto userid;
    }
    while(1){
        printf("Enter a password: ");
        fscanf(stdin, "%s", new_user->password);
        while(getchar() != '\n');
        if(strlen(new_user->password)<=6){
            break;
        }
        printf("Password must be six characters or less!\n");
    }
    printf("Enter the initial balance: ");
    fscanf(stdin, "%lf", &new_user->balance);
    while(getchar() != '\n');
    new_user->next = userList;
    return new_user;
}

/********************************************************************
 * This function will print the formatted customer data for all of  *
 * the customers in the linked list specified in no specific order. *
 ********************************************************************/
void printCustomers(struct user *userList){
    printf("First Name  Last Name         City  State      Phone      ID     Balance\n-------------------------------------------------------------------------\n");
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id,"admin")!= 0 ){
            printf("%10s ", p->first_name);
            printf("%10s ", p->last_name);
            printf("%12s ", p->city);
            printf("%6s ", p->state);
            printf("%10s ", p->phone);
            printf("%7s ", p->id);
            //printf("%6s ", p->password);
            printf("%10.2lf ", p->balance);
            printf("\n");
        } 
    }
}

/********************************************************************
 * This function will print the data for the customer in the linked *
 * list with the given user ID.                                     *
 ********************************************************************/
void printCustomerInfo(struct user *userList, char userId[]){
    int flag = 0;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, userId) == 0){
            printf("%s ", p->first_name);
            printf("%s ", p->last_name);
            printf("%s ", p->city);
            printf("%s ", p->state);
            printf("%s ", p->phone);
            printf("%s ", p->id);
            printf("%s ", p->password);
            printf("%.2lf ", p->balance);
            printf("\n");
            return;
        }
    }
        printf("No user found with that id.");
}

/********************************************************************
 * This function will delete the user with the specified user ID    *
 * from the linked list and will free the memory occupied by that   *
 * user. The admin user cannot be deleted, as this prevents the     *
 * admin menu from being accessed.                                  *
 ********************************************************************/
struct user *deleteCustomer (struct user *userList, char userId[]){
    struct user *prev = NULL;
    struct user *next = NULL;
    if (strcmp(userId, "admin") == 0){
        printf("Cannot delete admin user!\n");
        return userList;
    }
    for(struct user *p = userList; p != NULL; p = p->next){
        if (strcmp(p->id, userId) == 0){
            if (prev == NULL){
                next= p->next;
                free(p);
                printf("User deleted!\n");
                return next;
            }
            prev->next = p->next;
            free(p);
            printf("User deleted!\n");
            return userList;
        }
        prev=p;
    }
    printf("User not found!\n");
    return userList;
}

/******************************************************************
 * This function swaps the position of 2 names within the array   *
 * last_name_list. Accepts a pointer to the array and an integer  *
 * representing the index of the first of the 2 names.            *
 ******************************************************************/
void swap(char *last_name_list[], int i){
    char *temp = last_name_list[i];
    last_name_list[i] = last_name_list[i+1];
    last_name_list[i+1] = temp;
}

/**********************************************************************
 * This function accepts a pointer to the list of user's names,       *
 * and a number of names to be sorted. Every name in the list is      *
 * compared to the following name, and swapped if neccessary. This    *
 * process continues until the names are listed in alphabetical order *
 **********************************************************************/
void alphabetize(struct user *userList, int numCustomers){

    // Creates a 2D array of last names from the structure pointed to by userList
    struct user *p;
    int i = 0;
    char *last_name_list[numCustomers];
    for (p = userList; p != NULL; p = p->next){
        if(strcmp("admin", p->id) != 0){
            last_name_list[i] = p->last_name;
            i++;
        }
    }
    
    // Sorts the 2D array into alphabetical order
    while(TRUE){
        int flag = TRUE;
        for (int i = 0; i < numCustomers-1; i++){
            if (!(strcmp(last_name_list[i], last_name_list[i+1]) <= 0)){ // Names need to be swapped
                swap(last_name_list, i);
                flag = FALSE;
            }
            else {
                continue;
            }
        }
        if (flag){ // The list is in alphabetical order
            break;
        }    
    }

    // Prints the alphabetized list of accounts
    struct user *n;
    printf("First Name  Last Name         City  State      Phone      ID     Balance\n-------------------------------------------------------------------------\n");
    for (int i = 0; i <= numCustomers; i++){
        for (n = userList; n != NULL; n = n->next){
            if (n->last_name == last_name_list[i]){
                printf("%10s ", n->first_name);
                printf("%10s ", n->last_name);
                printf("%12s ", n->city);
                printf("%6s ", n->state);
                printf("%10s ", n->phone);
                printf("%7s ", n->id);
                //printf("%6s ", n->password);
                printf("%10.2lf ", n->balance);
                printf("\n");
            }
        }
    }
}

/*************************************************************************
 * This function sorts the customer accounts into ascending order based  *
 * on their account balances. The last 5 accounts are printed in reverse *
 * order (so that the balances are displayed in descending order). Uses  *
 * helper function SortArray to sort the list of account balances.       *
 *************************************************************************/
void top_five(struct user *userList, int numCustomers){
    struct user *p;
    double master[numCustomers];
    int i = 0;
    for (p = userList; p != NULL; p = p->next){
        if(strcmp("admin", p->id)!=0){
            master[i] = p->balance;
            i++;
        } 
    }
    printf("First Name  Last Name         City  State      Phone      ID     Balance\n-------------------------------------------------------------------------\n");    
    SortArray(master, 0, numCustomers);
    for (int j = numCustomers; j >= numCustomers-5; j--){
        struct user *m;
        for (m = userList; m != NULL; m = m->next){
            if (master[j] == m->balance){
                printf("%10s ", m->first_name);
                printf("%10s ", m->last_name);
                printf("%12s ", m->city);
                printf("%6s ", m->state);
                printf("%10s ", m->phone);
                printf("%7s ", m->id);
                //printf("%6s ", m->password);
                printf("%10.2lf ", m->balance);
                printf("\n");
            }
        }
    }
}

// Recursive function (the quicksort routine from the book) 
// Sorts the elements of master[] into ascending order
void SortArray(double master[], int low, int high){
    int middle;
    if (low >= high) return;
    middle = split(master, low, high);
    SortArray(master, low, middle-1);
    SortArray(master, middle+1, high);
}

// Helper function to SortArray, repeatedly divides
// master[] into sortable segments
int split(double master[], int low, int high){
    int part_element = master[low];

    for (;;) {
        while (low < high && part_element <= master[high])
            high--;
        if (low >= high) break;
        master[low++] = master[high];

        while (low < high && master[low] <= part_element)
            low++;
        if (low >= high) break;
        master[high--] = master[low];
    }
    master[high] = part_element;
    return high;
}

/********************************************************************
 * This function deletes a user sometimes created with empty data   *
 * due to formatting issues with the text file.                     *
 ********************************************************************/
struct user *deleteVoidUser(struct user *userList){
    struct user *prev = NULL;
    struct user *next = NULL;
    for(struct user *p = userList; p != NULL; p = p->next){
        if (p->balance == 0 && strcmp(p->id, "admin") != 0){
            if (prev == NULL){
                next= p->next;
                free(p);
                return next;
            }
            prev->next = p->next;
            free(p);
            printf("User deleted!\n");
            return userList;
        }
        prev=p;
    }
    return userList;
}