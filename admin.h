#include "user.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

struct user *pullData(char fileName[]);

struct user *createCustomer(struct user *userList);

void printCustomers(struct user *userList);

void printCustomerInfo(struct user *userList, char userId[]);

void writeData(char fileName[], struct user *userList);

struct user *deleteCustomer (struct user *userList, char userId[]);

void alphabetize(struct user *userList, int numCustomers);

void top_five(struct user *userList, int numCustomers);

int count_lines(FILE customer_file);

void set_first_name(struct user *userList, char userId[]);

void set_last_name(struct user *userList, char userId[]);

void set_city(struct user *userList, char userId[]);

void set_state(struct user *userList, char userId[]);

void set_phone(struct user *userList, char userId[]);

void set_id(struct user *userList, char userId[]);

void set_password(struct user *userList, char userId[]);

int userExists(struct user *userList, char userId[]);

