#ifndef USERH
#define USERH

struct user {
    char first_name[9];
    char last_name[9];
    char city[11];
    char state[3];
    char phone[9];
    char id[6];
    char password[7];
    double balance;
    struct user *next;
};

#endif