#include "admin.h"
#include "customer.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main (int argc, char *argv[]){


    char ID[6];
    char password[7];
    char customerId[6];
    char transferUser[6];
    char menuSelect;
    double amount;
    FILE *customer_file;

    customer_file = fopen(argv[1], "r+");
    int numCustomers = count_lines(*customer_file);
    fclose(customer_file);

    struct user *userList = pullData(argv[1]);
    //main menu
    restart: printf("\nWelcome to Online Banking/ATM system\n");
    printf("====================================\n\n");
    printf("Enter your Customer/Admin ID, or enter 0 to exit: ");
    scanf("%5s", ID);
    while(getchar() != '\n');
    if(strcmp(ID, "0")==0){
        writeData(argv[1], userList);
        printf("Goodbye!\n");
        return 0;
    }
    printf("Enter your Customer/Admin Password: ");
    scanf("%6s", password);
    while(getchar() != '\n');

    
    if (strcmp(ID, "admin") == 0){
        if (!login(userList,ID,password)){
            printf("Invalid login credentials!\n");
            goto restart;
        }
        else{
            //admin menu
            adminMenu: printf("-------------------------\n");
            printf("Administrator Menu\n");
            printf("-------------------------\n\n");
            printf("1) Create Customer Account\n");
            printf("2) Change password\n");
            printf("3) View Customer Info\n");
            printf("4) Change Customer Info\n");
            printf("5) Delete Customer Account\n");
            printf("6) Show Top 5 Accounts\n");
            printf("7) Show customer accounts alphabetically\n");
            printf("8) Exit\n");

            printf("Please make a selection: ");
            scanf("%1c", &menuSelect);
            while(getchar() != '\n');

            switch(menuSelect){
                case '1':
                    printf("Create a customer account selected. \n");
                    userList = createCustomer(userList);
                    goto adminMenu;
                case '2':
                    printf("Enter new password: ");
                    changePassword(userList, ID);
                    goto adminMenu;
                case '3': 
                    printf("View Customer Info:\nEnter a customer id: ");
                    scanf("%5s", customerId);
                    while(getchar() != '\n');
                    printCustomerInfo(userList, customerId);
                    goto adminMenu;
                case '4': //impl
                    printf("Change Customer Info: \n");
                    printf("View Customer Info:\nEnter a customer id: ");
                    scanf("%5s", customerId);
                    while(getchar() != '\n');
                    if(!userExists(userList, customerId)){
                        printf("User does not exist!\n");
                        goto adminMenu;
                    }
                    changeCustomerMenu: printf("-------------------------\n");
                                printf("Change Customer Data\n");
                                printf("-------------------------\n\n");
                                printf("1) Change First Name\n");
                                printf("2) Change Last Name\n");
                                printf("3) Change City\n");
                                printf("4) Change State\n");
                                printf("5) Change Phone Number\n");
                                printf("6) Change User ID\n");
                                printf("7) Change Password\n");
                                printf("8) Exit\n");

                                printf("Please make a selection: ");
                                scanf("%1c", &menuSelect);
                                while(getchar() != '\n');

                                switch(menuSelect){
                                    case '1':
                                        set_first_name(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '2':
                                        set_last_name(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '3': 
                                        set_city(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '4': 
                                        set_state(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '5':
                                        set_phone(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '6': 
                                        set_id(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '7':
                                        set_password(userList, customerId);
                                        goto changeCustomerMenu;
                                    case '8':
                                        printf("Returning to admin menu. \n\n");
                                        goto adminMenu;
                                    default:
                                        printf("Invalid menu selection\n");
                                        goto changeCustomerMenu;
                                }
                                        //goto adminMenu;
                case '5':
                    printf("Delete Customer Account: \n");
                    printf("Enter a customer id: ");
                    scanf("%5s", customerId);
                    while(getchar() != '\n');
                    userList = deleteCustomer(userList, customerId);
                    goto adminMenu;
                case '6': 
                    printf("Show Top 5 Accounts: \n");
                    top_five(userList, numCustomers);
                    goto adminMenu;
                case '7':
                    printf("Show accounts alphabetically: \n");
                    alphabetize(userList, numCustomers);
                    goto adminMenu;
                case '8':
                    printf("Returning to main menu. \n\n");
                    goto restart;
                default:
                    printf("Invalid menu selection\n");
                    goto adminMenu;
            }
        }
    }
    else if (login(userList,ID,password)){
                customerMenu:printf("-------------------------\n");
                printf("Customer Menu\n");
                printf("-------------------------\n\n");
                printf("a) Change Password\n");
                printf("b) View Customer Information\n");
                printf("c) View Balance\n");
                printf("d) Make a Deposit\n");
                printf("e) Transfer Money\n");
                printf("f) Withdraw Money\n");
                printf("g) Exit\n");

                printf("Please make a selection: ");
                scanf("%1c", &menuSelect);
                while(getchar() != '\n');

                switch(menuSelect){
                case 'a':
                    changePassword(userList, ID);
                    goto customerMenu;
                case 'b':
                    printf("View Customer Info:\n");
                    printCustomerInfo(userList, ID);
                    goto customerMenu;
                case 'c': 
                    printf("View Balance:\n");
                    printCustomerBalance(userList, ID);
                    goto customerMenu;
                case 'd': 
                    printf("Make a Deposit:\nEnter the amount to deposit: ");
                    scanf("%lf", &amount);
                    while(getchar() != '\n');
                    makeDeposit(userList, ID, amount);
                    goto customerMenu;
                case 'e':
                    printf("Transfer Money:\n");
                    printf("Enter user to transfer funds to: ");
                    scanf("%5s", transferUser);
                    while(getchar() != '\n');
                    printf("Enter the amount to transfer: ");
                    scanf("%lf", &amount);
                    while(getchar() != '\n');
                    makeTransfer(userList, ID, transferUser, amount);
                    goto customerMenu;
                case 'f': 
                    printf("Withdraw Money:\nEnter the amount to withdraw:");
                    scanf("%lf", &amount);
                    while(getchar() != '\n');
                    makeWithdrawal(userList, ID, amount);
                    goto customerMenu;
                case 'g':
                    printf("Returning to main menu \n\n");
                    goto restart;
                default:
                    printf("Invalid menu selection\n");
                    goto customerMenu;
            }      
    }
    else{
        goto restart;
    }
    return 1;
}