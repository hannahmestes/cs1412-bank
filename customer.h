#include "user.h"

int login(struct user *userList, char customerId[], char password[]);

void printCustomerBalance(struct user *userList, char userId[]);

void makeDeposit(struct user *userList, char userId[], double amount);

void makeWithdrawal(struct user *userList, char userId[], double amount);

void changePassword(struct user *userList, char userId[]);

void makeTransfer(struct user *userList, char sourceId[], char destId[], double amount);