HEADERS = customer.h admin.h

default: bank

bank: bank.o customer.o admin.o $(HEADERS)
	gcc -o bank bank.o customer.o admin.o

bank.o: bank.c customer.o admin.o $(HEADERS)
	gcc -o bank.o -c bank.c customer.o admin.o

admin.o: admin.c $(HEADERS)
	gcc -o admin.o -c admin.c

customer.o: customer.c $(HEADERS)
	gcc -o customer.o -c customer.c

